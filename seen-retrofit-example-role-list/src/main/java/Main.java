import java.io.IOException;

import ir.co.dpq.seen.PaginatedList;
import ir.co.dpq.seen.QueryParameter;
import ir.co.dpq.seen.Util;
import ir.co.dpq.seen.user.IRoleService;
import ir.co.dpq.seen.user.Role;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Main {

	public static void main(String[] args) throws IOException {
		// 1- Init retrofit
		String baseUrl = "http://www.webpich.com/api/";
		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(baseUrl)//
				.addConverterFactory(JacksonConverterFactory.create(Util.OBJECT_MAPPER))//
				.build();

		// 2- Create servicee
		IRoleService roleService = retrofit.create(IRoleService.class);

		// 3- call method
		QueryParameter params = new QueryParameter();
		PaginatedList<Role> list = roleService//
				.list(params.requestMap())//
				.execute()//
				.body();
		System.out.println("List of roles: " + list);
	}

}
