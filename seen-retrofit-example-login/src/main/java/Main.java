import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

import ir.co.dpq.seen.Util;
import ir.co.dpq.seen.user.IUserService;
import ir.co.dpq.seen.user.User;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Main {

	public static void main(String[] args) throws IOException {
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		OkHttpClient client = new OkHttpClient.Builder()//
				.cookieJar(new JavaNetCookieJar(cookieManager))//
				.build();

		// 1- Init retrofit
		String baseUrl = "http://www.webpich.com/api/";
		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(baseUrl)//
				.client(client)//
				.addConverterFactory(JacksonConverterFactory.create(Util.OBJECT_MAPPER))//
				.build();

		// 2- Create servicee
		IUserService userService = retrofit.create(IUserService.class);

		// 3- get user id
		LoginDialog lg = new LoginDialog(null);
		lg.setVisible(true);

		// 4- login
		Response<User> result = userService//
				.login(lg.getUsername(), lg.getPassword())//
				.execute();
		if (result.code() != 200) {
			System.out.println("Fail to login:" + result.errorBody());
			return;
		}

		// 5- get current user
		User user = userService//
				.getCurrentUser()//
				.execute()//
				.body();
		System.out.println("Current user is: " + user);

        System.exit(1);
	}

}
