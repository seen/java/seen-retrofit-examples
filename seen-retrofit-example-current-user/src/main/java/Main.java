import java.io.IOException;

import ir.co.dpq.seen.Util;
import ir.co.dpq.seen.user.IUserService;
import ir.co.dpq.seen.user.User;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Main {

	public static void main(String[] args) throws IOException {
		// 1- Init retrofit
		String baseUrl = "http://www.webpich.com/api/";
		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(baseUrl)//
				.addConverterFactory(JacksonConverterFactory.create(Util.OBJECT_MAPPER))//
				.build();

		// 2- Create servicee
		IUserService userService = retrofit.create(IUserService.class);
		
		// 3- call method
		User user = userService//
				.getCurrentUser()//
				.execute()//
				.body();
		System.out.println("Current user is: " + user);
	}

}
