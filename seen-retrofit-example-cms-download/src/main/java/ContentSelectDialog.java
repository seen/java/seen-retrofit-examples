import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import ir.co.dpq.seen.PaginatedList;
import ir.co.dpq.seen.QueryParameter;
import ir.co.dpq.seen.Util;
import ir.co.dpq.seen.cms.Content;
import ir.co.dpq.seen.cms.IContentService;
import ir.co.dpq.seen.user.IUserService;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ContentSelectDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField searchTextField;
	private JList<Content> list;
	private IContentService contentService;
	private QueryParameter queryParams;
	private Content content;
	private ContentProperties contentProperties;

	/**
	 * Launch the application.
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		OkHttpClient client = new OkHttpClient.Builder()//
				.cookieJar(new JavaNetCookieJar(cookieManager))//
				.build();

		// 1- Init retrofit
		String baseUrl = "http://www.webpich.com/api/";
		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(baseUrl)//
				.client(client)//
				.addConverterFactory(JacksonConverterFactory.create(Util.OBJECT_MAPPER))//
				.build();

		// 2- Create servicee
		IUserService userService = retrofit.create(IUserService.class);

		// 3- get user id
		LoginDialog lg = new LoginDialog(null);
		lg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		lg.setVisible(true);

		// 4- login
		userService//
				.login(lg.getUsername(), lg.getPassword())//
				.execute();

		try {
			IContentService contentService = retrofit.create(IContentService.class);
			ContentSelectDialog dialog = new ContentSelectDialog();
			dialog.contentService = contentService;
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ContentSelectDialog() {
		setBounds(100, 100, 597, 356);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JSplitPane splitPane = new JSplitPane();
			contentPanel.add(splitPane);
			{
				JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
				splitPane.setRightComponent(tabbedPane);
				{
					JPanel infoPanel = new JPanel();
					tabbedPane.addTab("Properties",
							new ImageIcon(
									ContentSelectDialog.class.getResource("/javax/swing/plaf/metal/icons/Inform.gif")),
							infoPanel, null);
					infoPanel.setLayout(new BorderLayout(0, 0));
					{
						contentProperties = new ContentProperties();
						infoPanel.add(contentProperties, BorderLayout.CENTER);
					}
				}
				{
					JPanel previewPanel = new JPanel();
					tabbedPane.addTab("Privew",
							new ImageIcon(ContentSelectDialog.class
									.getResource("/javax/swing/plaf/basic/icons/image-delayed.png")),
							previewPanel, null);
				}
			}
			{
				JPanel explorerPanel = new JPanel();
				splitPane.setLeftComponent(explorerPanel);
				explorerPanel.setLayout(new BorderLayout(0, 0));
				{
					JPanel panel = new JPanel();
					explorerPanel.add(panel, BorderLayout.NORTH);
					panel.setLayout(new FormLayout(
							new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
									FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
									FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
									FormSpecs.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("114px"), },
							new RowSpec[] { FormSpecs.LINE_GAP_ROWSPEC, RowSpec.decode("19px"), }));
					{
						JLabel lblSearch = new JLabel("Search");
						panel.add(lblSearch, "2, 2, right, default");
					}
					{
						searchTextField = new JTextField();
						panel.add(searchTextField, "4, 2, fill, top");
						searchTextField.setColumns(10);
					}
					{
						JButton button = new JButton(">>");
						button.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								searchContents();
							}
						});
						panel.add(button, "6, 2");
					}
				}
				{
					list = new JList<>();
					list.addListSelectionListener(new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent arg0) {
							Content content = list.getSelectedValue();
							setCurrentContent(content);
						}
					});
					list.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
					explorerPanel.add(new JScrollPane(list), BorderLayout.CENTER);
					list.setModel(new DefaultListModel<Content>());
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	void searchContents() {
		try {
			queryParams = new QueryParameter();
			PaginatedList<Content> page = contentService.list(queryParams.requestMap())//
					.execute()//
					.body();
			DefaultListModel<Content> model = (DefaultListModel<Content>) list.getModel();
			for (Content element : page.getItems()) {
				model.addElement(element);
			}
			list.updateUI();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void setCurrentContent(Content content) {
		this.content = content;
		contentProperties.setContent(this.content);
	}

}
