import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextPane;

import ir.co.dpq.seen.cms.Content;

public class ContentProperties extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextPane textPane;

	/**
	 * Create the panel.
	 */
	public ContentProperties() {
		setLayout(new BorderLayout(0, 0));

		textPane = new JTextPane();
		add(textPane, BorderLayout.CENTER);

	}

	public void setContent(Content content) {
		textPane.setText(content.toString());
	}
}
